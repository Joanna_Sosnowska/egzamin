from setuptools import setup

import os
#PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
# Put here required packages
packages = ['Django<=1.6', 'beautifulsoup4', 'requests', 'html5lib',]

if 'REDISCLOUD_URL' in os.environ and 'REDISCLOUD_PORT' in os.environ and 'REDISCLOUD_PASSWORD' in os.environ:
     packages.append('django-redis-cache')
     packages.append('hiredis')

setup(name='Egzam',
      version='1.0',
      description='OpenShift App',
      author='Joanna',
      author_email='example@example.com',
      url='https://pypi.python.org/pypi',
      install_requires=packages,
)

