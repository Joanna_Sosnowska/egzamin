__author__ = 'Joanna667'
from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^login$', views.log_in, name="login"),
    url(r'^logout$', views.log_out, name="logout"),
    url(r'^retard$', views.retard, name="retard"),
    url(r'^users/mygrades$', views.user, name="grades"),
    url(r'^users/mygrades/course/(?P<key>.+)$', views.download, name="course"),
    url(r'^', views.notfound,name='notfound')
)