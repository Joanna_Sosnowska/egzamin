# -*- coding: utf-8 -*-

__author__ = 'Joanna667'
from django.utils import timezone
from django.contrib.auth.models import User
from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, widget=forms.TextInput())
    password = forms.CharField(max_length=50, widget=forms.PasswordInput(render_value=False))