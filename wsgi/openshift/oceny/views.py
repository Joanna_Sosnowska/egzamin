#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Joanna667'


from forms import LoginForm
from django.shortcuts import render_to_response, redirect
from django.http import Http404
from django.core.urlresolvers import reverse
from email.mime.text import MIMEText
from django.core.mail import send_mail
import random, string
import json
from datetime import *

from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.views.decorators.cache import cache_page
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
#pamiec ustawiona w settings
from django.core.cache import cache
import getpass
import requests
import logging
from bs4 import BeautifulSoup
import sys
import os
from zipfile import ZipFile
from StringIO import StringIO 
import tempfile
from smtplib import SMTP
import html5lib
import settings

folder = settings.MEDIA_ROOT


def log_in(request):
    if request.user.is_authenticated():
        return index(request)

    if request.method == "GET":
        form = LoginForm()

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
                    name = form.cleaned_data['username']
                    passw = form.cleaned_data['password']
                    request.session['loginabc'] = name
                    request.session['passabc'] = passw
                    request.session.set_expiry(2*60)
                    request.session['logInToSite'] = True
                    exp = str(request.session.get_expiry_age())
                    return index(request, 'Udało się zalogować.')
        else:
            message = 'Niewłaściwy login i hasło.'

    return render_to_response('login.html', locals(), RequestContext(request))


def log_out(request):
    if request.user.is_authenticated():
        del request.session['loginabc']
        request.session['logInToSite'] = False
        cache.delete('user_data')
        return index(request, 'Wylogowano!')
    return index(request)


def index(request, message=''):
    logged = False
    try:
        request.session['loginabc']
        request.session['logInToSite']
        logged = True
    except:
        logged = False
    exp = str(request.session.get_expiry_age())

    return render_to_response("index.html", locals(), RequestContext(request))

def get_courses(user_data):
    sess = requests.session()
    sess.post('https://canvas.instructure.com/login', data=user_data)
    stronka = sess.get('https://canvas.instructure.com/grades')
    html = stronka.text
    zupa = BeautifulSoup(html, 'html5lib')
    table = zupa.find(id='content').find('tbody')
    kursy = []
    for th in table.find_all('tr'):
        td = th.find('td', 'course').get_text()
        klucz = th.find('a')['href']
        kursy.append([str(td), klucz[9:15]])
    return kursy

def get_grades(user_data, key):
    sess = requests.session()
    sess.post('https://canvas.instructure.com/login', data=user_data)
    strona = sess.get('https://canvas.instructure.com/courses/%s/grades'%str(key))
    html = strona.text
    zupa = BeautifulSoup(html, 'html5lib')
    table = zupa.find(id='grades_summary').find('tbody')
    oceny = []
    oceny.append(['Laboratoria', []])
    oceny.append(['Prace domowe', []])
    oceny.append(['Aktywność', []])
    oceny.append(['Egzamin', []])
    for th in table.find_all('tr', 'student_assignment'):
        div = th.find('div', 'context')
        try:
            div = div.get_text()
            if div == u'Laboratoria':
                ocenka=th.find('span', 'score').text
                podpis = th.find('a').text
                oceny[0][1].append([podpis, ocenka])
            elif div == u'Prace domowe':
                ocenka=th.find('span', 'score').text
                podpis = th.find('a').text
                oceny[1][1].append([podpis, ocenka])
            elif div == u'Aktywność':
                ocenka = th.find('span', 'score').text
                podpis = th.find('a').text
                oceny[2][1].append([podpis, ocenka])
            elif div == u'Egzamin':
                ocenka = th.find('span', 'score').text
                podpis = th.find('a').text
                oceny[3][1].append([podpis, ocenka])

        except:
            pass
    return oceny


#@cache_page(60 * 15)
def user(request, username="Admin"):
    try:
        user1 = request.session['loginabc']
    except:
        exp=0
        cache.delete('user_data')
        return index(request, 'Sesja wygasła - zaloguj się jeszcze raz!')

    user_data = {
        'pseudonym_session[unique_id]': request.session['loginabc'],
        'pseudonym_session[password]': request.session['passabc']
    }
    cache.set('user_data', user_data)
    try:
        exp = str(request.session.get_expiry_age())
        kursy = get_courses(cache.get('user_data'))
    except:
        return index(request, 'Błędne dane systemu canvas lub sesja wygasła')
    return render_to_response('courses.html', locals(), RequestContext(request))


@cache_page(60 * 5)
def download(request, key):
    try:
        user1 = request.session['loginabc']
    except:
        exp=0
        cache.delete('user_data')
        return index(request, 'Sesja wygasła - zaloguj się jeszcze raz!')
    try:
        oceny = get_grades(cache.get('user_data'), key)
    except:
        return index(request, 'Błędne dane systemu canvas lub sesja wygasła')
    tekst = "Moje oceny \n"
    tekst += "Kurs %s \n"%str(key)
    #tekst +=str(oceny)
    #tekst += json.dumps(oceny)
    for f in oceny:
        tekst += str(f[0])
        tekst += "\n"
        for f1 in f[1]:
            tekst += str('\t%s - %s \n' % ((f1[0]).encode('utf-8'), (f1[1]).encode('utf-8)')))
    #pliki tymczasowe
    in_memory = StringIO()
    zip = ZipFile(in_memory, "a")
    zip.writestr("grades.txt", tekst)
    #zip.writestr("grades.csv", tekst1)

    # fix for Linux zip files read in Windows
    for file in zip.filelist:
        file.create_system = 0

    zip.close()

    response = HttpResponse(mimetype="application/zip")
    response["Content-Disposition"] = "attachment; filename=my_grades.zip"

    in_memory.seek(0)
    response.write(in_memory.read())

    return response


@cache_page(60 * 20)
def notfound(request):
    info = 'Nie znaleziono strony'
    exp = str(request.session.get_expiry_age())
    return render_to_response('notfound.html', locals(), RequestContext(request))


def retard(request):
    try:
        user1 = request.session['loginabc']
        request.session.set_expiry(2*60)
        message = 'Sesja przedłuzona'
        exp = str(request.session.get_expiry_age())
    except:
        message = 'Sesja wygasła'
        exp = 0
    return render_to_response('index.html', locals(), RequestContext(request))
